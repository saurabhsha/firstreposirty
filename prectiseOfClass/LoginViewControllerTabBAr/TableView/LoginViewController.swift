//
//  LoginViewController.swift
//  prectiseOfClass
//
//  Created by Dev on 24.12.20.
//  Copyright © 2020 Saurabh. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    struct objects {
        var sectionName: String
        var sectionObjects: [String]!
        var image:UIImage
    }
    var objectArray = [objects]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Table"
      objectArray = [objects(sectionName: "section 1", sectionObjects: ["ksdfvhoivihiksan ksdfvhoivihiksan ksdfvhoivihiksan ksdfvhoivihiksan ksdfvhoivihiksan ksdfvhoivihiksan", "iufoerwvne", "kjecjsvnloe", "kjewbvnl", "2oi3hdp2", "12ehdoewi", "21ndlkw"], image: #imageLiteral(resourceName: "P5")),
                     objects(sectionName: "section 2", sectionObjects: ["ksdfvhoivihiksan", "iufoerwvne iufoerwvne iufoerwvne iufoerwvne iufoerwvne iufoerwvne iufoerwvne ", "kjecjsvnloe", "kjewbvnl", "2oi3hdp2", "12ehdoewi", "21ndlkw"],image: #imageLiteral(resourceName: "P2")),
        objects(sectionName: "section 3", sectionObjects: ["ksdfvhoivihiksan", "iufoerwvne", "kjecjsvnloe kjecjsvnloe kjecjsvnloe kjecjsvnloe kjecjsvnloe kjecjsvnloe kjecjsvnloe ", "kjewbvnl", "2oi3hdp2", "12ehdoewi", "21ndlkw"], image: #imageLiteral(resourceName: "P9")),
        objects(sectionName: "section 4", sectionObjects: ["ksdfvhoivihiksan", "iufoerwvne", "kjecjsvnloe", "kjewbvnl kjewbvnl kjewbvnl kjewbvnl kjewbvnl kjewbvnl kjewbvnl kjewbvnl ", "2oi3hdp2", "12ehdoewi", "21ndlkw"], image: #imageLiteral(resourceName: "P3")),
        objects(sectionName: "section 5", sectionObjects: ["ksdfvhoivihiksan", "iufoerwvne", "kjecjsvnloe", "kjewbvnl", "2oi3hdp2 2oi3hdp2 2oi3hdp2 2oi3hdp2 2oi3hdp2 2oi3hdp2 2oi3hdp2 2oi3hdp2 2oi3hdp2", "12ehdoewi", "21ndlkw"], image: #imageLiteral(resourceName: "P4")),
        objects(sectionName: "section 6", sectionObjects: ["ksdfvhoivihiksan", "iufoerwvne", "kjecjsvnloe", "kjewbvnl", "2oi3hdp2", "12ehdoewi 12ehdoewi 12ehdoewi 12ehdoewi 12ehdoewi 12ehdoewi 12ehdoewi 12ehdoewi", "21ndlkw"], image: #imageLiteral(resourceName: "P5"))]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectArray[section].sectionObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as! TableViewCell
        cell.cellLAbleText.text = objectArray[indexPath.section].sectionObjects[indexPath.row]
        cell.nextButten.tag = indexPath.row
        cell.nextButten.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return objectArray.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return objectArray[section].sectionName
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let loginVc = storyboard?.instantiateViewController(withIdentifier: "NextScreenViewController") as! NextScreenViewController
        loginVc.strLblText = objectArray[indexPath.section].sectionObjects[indexPath.row]
        loginVc.imgStr = objectArray[indexPath.section].image
        self.navigationController?.pushViewController(loginVc, animated: true)
    }
    @objc func buttonAction(sender: UIButton) {
        let data = IndexPath(row:sender.tag, section: 0)
        let loginVc = storyboard?.instantiateViewController(withIdentifier: "NextScreenViewController") as! NextScreenViewController
         loginVc.strLblText = objectArray[data.section].sectionObjects[data.row]
        loginVc.imgStr = objectArray[data.section].image
         self.navigationController?.pushViewController(loginVc, animated: true)
        
    }
   
        
//loginVc.strLblText = objectArray[indexPath.section].sectionObjects[indexPath.row]
//MARK:- Why this not go on butten click with data
//MARK:- Why my screen goes up when i click on any text field
        
        
//    func moveToNext(){
//        let loginVc = storyboard?.instantiateViewController(withIdentifier: "NextScreenViewController") as! NextScreenViewController
//        self.navigationController?.pushViewController(loginVc, animated: true)
//
//    }

     @IBAction func logOutButton(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        
     }
}
