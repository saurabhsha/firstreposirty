//
//  NextScreenViewController.swift
//  prectiseOfClass
//
//  Created by Dev on 25.12.20.
//  Copyright © 2020 Saurabh. All rights reserved.
//

import UIKit

class NextScreenViewController: UIViewController {
    
    @IBOutlet weak var lableText: UILabel!
    @IBOutlet weak var lable2Text: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    var strLblText: String?
    var imgStr:UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "New View"
        
        lableText.text = strLblText
        imgView.image = imgStr
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
