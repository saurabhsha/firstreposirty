//
//  ViewController.swift
//  prectiseOfClass
//
//  Created by Dev on 24.12.20.
//  Copyright © 2020 Saurabh. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var emailTextFiled: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var textLable: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("in view controller")
        self.navigationItem.title = "Login"
        // listen for keywird event
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keywordWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
//        //NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
//        //NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
//    }

    @IBAction func logInbutton(_ sender: UIButton) {
        let mail = emailTextFiled.text
        let password = passwordTextField.text
        
        if ( password == "")&&(mail == "") {
            showAlert(massage: "Plaese Fill Email & Password first", title: "Alert")
            return
        }
        if ( mail == ""){
            showAlert(massage: "Plaese Fill Email first", title: "Alert")
            return
        }
        if ( password == ""){
            showAlert(massage: "Plaese Fill Password first", title: "Alert")
            return
        } else if (password != nil)&&( mail != nil){
            //moveToNext()
        } else {
            showAlert(massage: "Given Email and Password is invelid", title: "Alert")
            return
        }
//        textFiled.endEditing(true)
//        ptextFiled.endEditing(true)
//        Ttextield.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.endEditing(true)

        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func showAlert(massage: String, title: String) {
        let al = UIAlertController.init(title: title, message: massage, preferredStyle: UIAlertController.Style.alert)
        al.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(al, animated: true, completion: nil)
    }
//    func moveToNext(){
//        let loginVc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        self.navigationController?.pushViewController(loginVc, animated: true)
//    }
//
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
//    @objc func keywordWillChange(notification: Notification){
//        guard let keywordRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey]) as? NSValue else {
//            return
//        }
//        if notification.name == UIResponder.keyboardWillShowNotification ||
//            notification.name == UIResponder.keyboardWillChangeFrameNotification{
//            view.frame.origin.y = -keywordRect.cgRectValue.height
//        } else {
//            view.frame.origin.y = 0
//        }
//    }
//    @objc func keywordWillBAckChange(notification: Notification){
//        view.frame.origin.y = 0
//    }
    
}



